(function () {
    'use strict';

    service.factory('ligasuperplayertransfersService', ligasuperService);
    function ligasuperService($http, $q,localStorageService) {

        var service = {};

        service.getTransfers = function (fixture_id) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'players_transfer?show=1&highlight=1',
                method: "GET",
                headers: {
                    'Authorization': 'Bearer '+localStorageService.get('token')
                }
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        

 

        return service;

    }
})();
