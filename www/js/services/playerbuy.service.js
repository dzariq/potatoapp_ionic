(function () {
    'use strict';


    service.factory('playerbuyservice', function ($filter, $q, teamvalidationService, $ionicPopup) {


        var data = []
        var teamvalue = 0

        return {
            getPlayers: function () {
                console.log(data)
                if (data.length == 0) {
                    this.resetPlayers();
                }
                return data;
            },
            resetPlayers: function () {
                data = [
                    {
                        'position': 'GK',
                        'name': ''
                    },
                    {
                        'position': 'GK',
                        'name': ''
                    },
                    {
                        'position': 'DF',
                        'name': ''
                    },
                    {
                        'position': 'DF',
                        'name': ''
                    },
                    {
                        'position': 'DF',
                        'name': ''
                    },
                    {
                        'position': 'DF',
                        'name': ''
                    },
                    {
                        'position': 'DF',
                        'name': ''
                    },
                    {
                        'position': 'MF',
                        'name': ''
                    },
                    {
                        'position': 'MF',
                        'name': ''
                    },
                    {
                        'position': 'MF',
                        'name': ''
                    },
                    {
                        'position': 'MF',
                        'name': ''
                    },
                    {
                        'position': 'MF',
                        'name': ''
                    },
                    {
                        'position': 'ST',
                        'name': ''
                    },
                    {
                        'position': 'ST',
                        'name': ''
                    },
                    {
                        'position': 'ST',
                        'name': ''
                    },
                    {
                        'position': 'ST',
                        'name': ''
                    },
                ];

            },
            setPlayer: function (player, playertoreplace) {
                var r = $q.defer();
                var keepGoing = true;
                teamvalidationService.validateTeam(data, player, '',playertoreplace).then(function () {
                    var checkdup = $filter('filter')(data, {'id': player.id})
                    if (checkdup.length > 0) {

                        r.reject(player.name + ' is already in your team');

                        return false;
                    }

                    angular.forEach(data, function (value, key) {
                        if (keepGoing) {
                            if (playertoreplace == value.id) {
                                data[key] = player;
                                keepGoing = false;
                                r.resolve(player.name + ' is now added to your team');
                            } else if (playertoreplace == 0 && value.name == '' && value.position == player.position) {
                                data[key] = player;
                                keepGoing = false;
                                r.resolve(player.name + ' is now added to your team');
                            }
                        }
                    });
                    if (player.position == 'GK') {
                        var posdis = 'Goalkeepers';
                    }
                    if (player.position == 'DF') {
                        var posdis = 'Defenders';
                    }
                    if (player.position == 'MF') {
                        var posdis = 'Midfielders';
                    }
                    if (player.position == 'ST') {
                        var posdis = 'Forwards';
                    }
                    if (keepGoing) {
                        r.reject('You already have maximum number of ' + posdis);
                    }
                }, function (error) {
                    r.reject(error);
                })

                return r.promise;
            },
            setValue: function (value) {
                teamvalue = value
            },
            getValue: function () {
                return teamvalue;
            }

        };
    });

})();
