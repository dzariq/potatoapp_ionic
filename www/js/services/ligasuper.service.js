(function () {
    'use strict';

    service.factory('ligasuperService', ligasuperService);
    function ligasuperService($http, $q) {

        var service = {};

        service.getRules = function (fixture_id) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'fantasy_rules',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        service.getFixtures = function (fixture_id) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'fixtures/'+fixture_id,
                method: "GET"
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        service.getEntries = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'entry',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getEntry = function (id) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'entryid/'+id,
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayer = function (playerid) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'player/'+playerid,
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayerStats = function (playerid) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'player_stats/'+playerid,
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getTeam = function (teamid) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'teams/'+teamid,
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getTeams = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'teams',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayers = function (param) {
            var m = $q.defer();
                      param = param || {};
            $http({
                url: ligasuper_path + 'players',
                method: "GET",
                params: param
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getTeamStandings = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'reports/teamstandings',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayerScores = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'reports/playerscores?limit=5',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayerAssists = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'reports/playerassists?limit=5',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };
        service.getPlayerPopular = function () {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'reports/playerpopular?limit=10',
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };

        service.getAllFixtures = function (season,matchweekid) {
            var m = $q.defer();
            $http({
                url: ligasuper_path + 'fixtures?season='+season+'&matchweek='+matchweekid,
                method: "GET",
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        };

 

        return service;

    }
})();
