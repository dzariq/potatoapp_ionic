(function () {
    'use strict';

    service.factory('formationService', formationService);
    function formationService($http, $q, $filter) {

        var service = {};

        service.setFormationLayout = function (formation_name, playersraw, lineup) {
            var r = $q.defer();
            console.log(playersraw)

            var players = [];

            if (playersraw) {
                jQuery.each(playersraw, function (i, item) {
                    players.push(item.player)
                })
            }


            if (formation_name == '442') {
                var dfCount = 4
                var mfCount = 4
                var stCount = 2
            }
            if (formation_name == '451') {
                var dfCount = 4
                var mfCount = 5
                var stCount = 1
            }
            if (formation_name == '433') {
                var dfCount = 4
                var mfCount = 3
                var stCount = 3
            }
            if (formation_name == '343') {
                var dfCount = 3
                var mfCount = 4
                var stCount = 3
            }
            if (formation_name == '352') {
                var dfCount = 3
                var mfCount = 5
                var stCount = 2
            }

            var gk = [];
            var df = [];
            var mf = [];
            var st = [];
            var subgk = [];
            var sub = [];

            console.log(lineup)

            if (lineup != 0) {
                console.log(lineup.gk)
                gk = angular.copy(lineup.gk)
                subgk = angular.copy(lineup.subgk)
                sub = angular.copy(lineup.sub)
                df = angular.copy(lineup.df)
                mf = angular.copy(lineup.mf)
                st = angular.copy(lineup.st)
            } else {
                var gks = players.filter(function (player) {
                    return (player.position == "GK");
                });
                var dfs = players.filter(function (player) {
                    return (player.position == "DF");
                });
                var mfs = players.filter(function (player) {
                    return (player.position == "MF");
                });
                var sts = players.filter(function (player) {
                    return (player.position == "ST");
                });



                console.log(gks)
                jQuery.each(gks, function (i, item) {
                    if (i == 0) {
                        item.parent = 'gk';
                        item.type = 1;
                        item.index = 0;
                        item.inteam = true;
                        gk.push(angular.copy(item))
                    } else if (i == 1) {
                        item.parent = 'subgk';
                        item.type = 1;
                        item.index = 0;
                        item.inteam = true;
                        subgk.push(angular.copy(item))
                    }
                })


                for (var i = 0; i < dfCount; i++) {
                    dfs[i].parent = 'df'
                    dfs[i].type = 1
                    dfs[i].index = i
                    dfs[i].inteam = true
                    df.push(angular.copy(dfs[i]))
                    if (i == dfCount - 1) {
                        //remaining df to push to sub
                        var left = dfs.length - dfCount;
                        for (var m = 1; m <= left; m++) {
                            dfs[i + m].parent = 'sub'
                            dfs[i + m].type = 1
                            dfs[i + m].index = i + m
                            dfs[i + m].inteam = true
                            sub.push(angular.copy(dfs[i + m]))
                        }
                    }
                }
                for (var i = 0; i < mfCount; i++) {
                    mfs[i].parent = 'mf'
                    mfs[i].type = 1
                    mfs[i].index = i
                    mfs[i].inteam = true
                    mf.push(angular.copy(mfs[i]))
                    if (i == mfCount - 1) {
                        //remaining df to push to sub
                        var left = mfs.length - mfCount;
                        for (var m = 1; m <= left; m++) {
                            mfs[i + m].parent = 'sub'
                            mfs[i + m].type = 1
                            mfs[i + m].index = i + m
                            mfs[i + m].inteam = true
                            sub.push(angular.copy(mfs[i + m]))
                        }
                    }
                }
                for (var i = 0; i < stCount; i++) {
                    sts[i].parent = 'st'
                    sts[i].type = 1
                    sts[i].index = i
                    sts[i].inteam = true
                    st.push(angular.copy(sts[i]))
                    if (i == stCount - 1) {
                        //remaining df to push to sub
                        var left = sts.length - stCount;
                        for (var m = 1; m <= left; m++) {
                            sts[i + m].parent = 'sub'
                            sts[i + m].type = 1
                            sts[i + m].index = i + m
                            sts[i + m].inteam = true
                            sub.push(angular.copy(sts[i + m]))
                        }
                    }
                }

                jQuery.each(sub, function (i, item) {
                    item.index = i
                })

            }

            var data = {
                'gk': gk,
                'df': df,
                'mf': mf,
                'st': st,
                'subgk': subgk,
                'sub': sub,
            }
            console.log(data)
            r.resolve(data);
//            m.reject();

            return r.promise;
        }
        service.getFormations = function (param) {
            var m = $q.defer();
            var formations = [
                {
                    'name': '442',
                    'value': 'partials/ligasuper/fantasy/pickteam_formation.html?version=1',
                },
                {
                    'name': '433',
                    'value': 'partials/ligasuper/fantasy/pickteam_formation.html?version=1',
                },
                {
                    'name': '451',
                    'value': 'partials/ligasuper/fantasy/pickteam_formation.html?version=1',
                },
                {
                    'name': '343',
                    'value': 'partials/ligasuper/fantasy/pickteam_formation.html?version=1',
                },
                {
                    'name': '352',
                    'value': 'partials/ligasuper/fantasy/pickteam_formation.html',
                },
            ]
            m.resolve(formations);
//            m.reject();

            return m.promise;
        };

        return service;

    }
})();
