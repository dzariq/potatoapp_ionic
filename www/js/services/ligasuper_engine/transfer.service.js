(function () {
    'use strict';

    service.factory('transferService', transferService);
    function transferService($http, $q) {

        var service = {};


        service.makeTransfer = function (data, teamid) {
            var m = $q.defer();
            data = data || {};
            $http({
                url: ligasuper_path + 'entry/transfer/' + teamid,
                method: "POST",
                data: data,
            }).success(function (response) {
                    m.resolve(response);

            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
        }



        return service;

    }
})();
