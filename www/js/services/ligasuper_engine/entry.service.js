service.factory('entryService', function ($http, $q) {

    var service = {};

    service.getPlayers = function (teamid) {
        var m = $q.defer();

        $http({
            url: ligasuper_path + 'entry/players/' + teamid,
            method: "GET",
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }

    service.myTransfers = function (teamid) {
        var m = $q.defer();

        $http({
            url: ligasuper_path + 'entry/transfers/' + teamid,
            method: "GET",
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }



    service.getPicks = function (teamid, matchweek) {
        var m = $q.defer();

        $http({
            url: ligasuper_path + 'entry/picks/' + teamid + '?matchweek=' + matchweek,
            method: "GET",
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }

    service.getTeam = function (teamid) {
        var m = $q.defer();

        $http({
            url: ligasuper_path + 'entryidone/' + teamid,
            method: "GET",
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }

    service.confirmTeam = function (data, teamid) {
        var m = $q.defer();
        data = data || {};
        $http({
            url: ligasuper_path + 'confirmteam/' + teamid,
            method: "PUT",
            data: data,
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }

    service.confirmPicks = function (data, teamid, matchweek) {
        var m = $q.defer();
        data = data || {};
        $http({
            url: ligasuper_path + 'confirmpicks/' + teamid + '?matchweek=' + matchweek,
            method: "PUT",
            data: data,
        }).success(function (response) {
            m.resolve(response);
        }).catch(function (result) {
            m.reject(result);
        });
        return m.promise;
    }

    return service;

});