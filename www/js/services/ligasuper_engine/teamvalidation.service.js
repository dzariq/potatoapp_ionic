(function () {
    'use strict';

    service.factory('teamvalidationService', teamvalidationService);
    function teamvalidationService($http, $q, $filter) {

        var service = {};


        service.validateTeam = function (players, newplayer, index, playertoreplace) {
            var r = $q.defer();
            var cart = angular.copy(players)
            var newp = angular.copy(newplayer)

            console.log(cart)
            if (newp !== '') {
                cart.push(newp)
            } else if (index !== '') {
                cart.splice(index, 1)
            }

            if (playertoreplace != 0) {
                angular.forEach(cart, function (item, i) {
                    if(item.id == playertoreplace){
                        cart.splice(i,1)
                    }
                })
            }

            //total team value
            var teamValue = 0;

            var maxgk = 2;
            var maxdf = 5;
            var maxmf = 5;
            var maxst = 4;

            var gkcount = 0
            var dfcount = 0
            var mfcount = 0
            var stcount = 0
            console.log(cart)
            var sameteam = {}
            angular.forEach(cart, function (item, i) {
                if (item.name != '') {
                    if (sameteam[item.team.name] == undefined) {
                        sameteam[item.team.name] = 1;
                    } else {
                        sameteam[item.team.name] = sameteam[item.team.name] + 1
                    }
                    if (item.position == 'GK') {
                        gkcount++;
                    }
                    if (item.position == 'DF') {
                        dfcount++;
                    }
                    if (item.position == 'MF') {
                        mfcount++;
                    }
                    if (item.position == 'ST') {
                        stcount++;
                    }
                    teamValue = parseFloat(teamValue) + parseFloat(item.value)
                }
            })

            console.log(sameteam)


            if (gkcount > maxgk) {
                r.reject('You are only allowed to have ' + maxgk + ' Goalkeepers');

            }
            if (dfcount > maxdf) {
                r.reject('You are only allowed to have ' + maxdf + ' Defenders');
            }
            if (mfcount > maxmf) {
                r.reject('You are only allowed to have ' + maxmf + ' Midfielders');
            }
            if (stcount > maxst) {
                r.reject('You are only allowed to have ' + maxst + ' Strikers');
            }
//            if (cart.length > 16) {
//                r.reject('You are only allowed to have 16 Players');
//            }

            console.log(sameteam)

            angular.forEach(sameteam, function (item, i) {
                console.log(i)
                console.log(item)
                if (item > 3) {
                    r.reject('You are only allowed to have maximum of 3 Players from ' + i);
                }
            });

            var data = {
                'teamValue': teamValue
            }
            r.resolve(data)
            return r.promise;
        }
//            m.reject();



        return service;

    }
})();
