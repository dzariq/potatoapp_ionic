service.factory('AuthService', function ($q, $http) {
    this.login = function (data, callback, err) {
        console.log(data)
        $http({
            url: adminurl + 'auth/login',
            method: "POST",
            data: data
        }).success(callback).error(err);
    }
    this.signup = function (data, callback, err) {
        console.log(data)
        $http({
            url: adminurl + 'auth/register',
            method: "POST",
            data: data
        }).success(callback).error(err);
    }
    this.logout = function (data, callback, err) {
        console.log(data)
       data = data || {};
            var m = $q.defer();
            $http({
                url: adminurl + 'admin/auth/logout',
                method: "POST",
                headers: {
                    'Authorization': 'Bearer ' + $.jStorage.get("token")

                }
            }).success(function (response) {
                m.resolve(response);
            }).catch(function (result) {
                m.reject(result);
            });
            return m.promise;
    }

    return this;
});
