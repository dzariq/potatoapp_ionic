﻿app.controller('AuthCtrl', function ($scope,$ionicSideMenuDelegate, logged, $ionicHistory, NotificationSyncService, $window, $state, $location, $ionicLoading, AuthService, $ionicPopup, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform) {
                            
    var vm = this;
    vm.loginData = {};
    vm.signupData = {};
    vm.profilesubmitted = 0;
    vm.signupData.verified = 0;
    vm.loginData.device_id = "";
    $ionicSideMenuDelegate.canDragContent(false);

    if ($.jStorage.get("registration_pending", 0) != 0) {
        vm.profilesubmitted = 1;
        vm.signupData = $.jStorage.get("registration_pending", 0)
    }

    vm.resetSignUp = function () {
        vm.profilesubmitted = 0;
        vm.signupData.verified = 0;
        $.jStorage.set("registration_pending", 0)
    }
    vm.randomString = function (length, chars) {
        var result = '';
        for (var i = length; i > 0; --i)
            result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    $ionicPlatform.ready(function () {
        if (window.cordova) {
            // running on device/emulator
            vm.loginData.device_id = device.uuid;

        } else {
            // running in dev mode
            vm.loginData.device_id = '1234';

        }
    })

    vm.signuppage = function () {
        $location.path('/popupgeneral/signup')
    }

    vm.loginpage = function () {
        $location.path('/popupgeneral/login')
    }

    vm.signup = function () {
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });
        console.log(vm.signupData)
        if (vm.profilesubmitted == 0) {
            vm.signupData.code = vm.randomString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        } else {
            if (vm.signupData.code != vm.signupData.codecheck) {
                $ionicLoading.hide()
                vm.showPopup('Oops..', 'Invalid Verification Code')
                return false
            }
        }
        vm.signupData.device_id = '1234'
        AuthService.signup(vm.signupData, signupsuccess, function (err, status) {
            $ionicLoading.hide();
            vm.showPopup('Oops..', err.result.message)
        });
    }

    vm.login = function () {
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });
        AuthService.login(vm.loginData, loginsuccess, function (err, status) {
            $ionicLoading.hide();
            vm.showPopup('Oops..', err.result.message)

        });
    }


    var signupsuccess = function (data, status) {
        vm.showPopup('Welcome ' + data.result.firstname)
        $.jStorage.set("profile", JSON.stringify(data.result.profile));
        $.jStorage.set("token", data.result.token);
        $ionicLoading.hide();

    }

    var loginsuccess = function (data, status) {
        $ionicLoading.hide();

        console.log(data.result)
        vm.showPopup('Welcome ' + data.result.profile.firstname)
        $.jStorage.set("profile", JSON.stringify(data.result.profile));
        $.jStorage.set("token", data.result.token);
        $.jStorage.set("entry_id", data.result.profile.lsm_entry.entry_id);
        $.jStorage.set("logged", 1);

        $ionicLoading.hide();
        if (window.cordova) {
            FCMPlugin.subscribeToTopic('match_update');
        }
        $location.path('/app/home')
    };
    var successfcm = function (data) {
        console.log(data)
    }
    var errorfcm = function (data) {
        console.log(data)
    }
    vm.showPopup = function (title, msg) {
        $ionicPopup.alert({
            title: title,
            template: msg,
            buttons: [{
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                }]
        });
    };
    ionicMaterialInk.displayEffect();
});