﻿app.controller('TransferCtrl', function (transferService, ligasuperService, toaster, player, myteam, playerbuyservice, teams, $stateParams, $ionicLoading, $ionicPopup, $timeout, $location, $scope, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform, NotificationSyncService, NotificationService) {

    $scope.data = player
    $scope.teams = teams
    $scope.newplayer = {}
    $scope.bank = myteam.bank
    $scope.transfer_left = myteam.transfer_left
    $scope.filter_position = player.position


    $scope.lineup = function () {
        $location.path('/app/lineup')
    }

    $scope.buyPlayer = function (newplayer) {
        $scope.newplayer = newplayer
        $scope.newbank = parseFloat($scope.bank) + (parseFloat($scope.data.value) - parseFloat($scope.newplayer.value))
    }

    $scope.teamfilter = ''
    $scope.orderby = '-points'
    $scope.teamvalue = 0;

    $scope.buyplayer = function (newplayer) {
        $scope.newplayer = newplayer
        $scope.newbank = parseFloat($scope.bank) + (parseFloat($scope.data.value) - parseFloat($scope.newplayer.value))
    }
    
    $scope.goPlayers = function () {
        var data = {
            'position': $scope.filter_position,
            'team_id': $scope.filter_team,
            'value': $scope.filter_value,
            'entry_id': myteam.entry_id
        }
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });
        ligasuperService.getPlayers(data).then(function (result) {
            $scope.playerlist = result.result;
            $ionicLoading.hide()
        }, function (e) {
            $ionicLoading.hide()

        })
    }

    $scope.goPlayers()

    $scope.confirmTransfer = function () {
        var r = confirm("Are you sure to swap these players?");
        if (r == true) {
            var data = {
                'player': player.id,
                'newplayer': $scope.newplayer.id
            }
            $scope.loading = true;

            transferService.makeTransfer(data, myteam.entry_id).then(function (result) {
                $scope.loading = false;
                toaster.pop('Lineup', "Congratulations", "You have made a successful player transfer");
                window.location.reload()

            }, function (error) {
                $scope.loading = false;
                console.log(error.data.result.message)
                toaster.pop('Lineup', "Oops..", error.data.result.message);
            })
        } else {
            return false
        }

    }
});