service.factory('HomeService', function ($q,$http, $cordovaSQLite) {
    this.all = function (category_id) {
        if (window.cordova) {
            console.log("Opening database on device");
            var db = $cordovaSQLite.openDB("minicms.db");
        } else {
            console.log("Opening database on web");
            var db = window.openDatabase("minicms.db", '1', 'my', 1024 * 1024 * 100);
        }
        var q = $q.defer();
        $cordovaSQLite.execute(db, "SELECT * FROM notification ").then(function (result) {
            var data = [];
            for (var i = 0; i < result.rows.length; i++) {
                data.push(result.rows.item(i));
            }
            q.resolve(data)
        });

        return q.promise;
    }
    

    return this;
});
