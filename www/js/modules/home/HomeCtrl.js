﻿app.controller('HomeCtrl', function (entryService, playerbuyservice, $ionicLoading, myteam, $ionicPopup, $timeout, $location, $scope, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform, NotificationSyncService, NotificationService) {

    var vm = this;
    $scope.myplayers = myplayers
    $scope.myteam = myteam

    //get picks
    entryService.getPicks($.jStorage.get("entry_id"), $.jStorage.get("current_mw")).then(function (response) {
    }, function (error) {
    })
    //get picks

    $scope.buyplayer = function (pos, playertoreplace) {

        $location.path('/popupgeneral/buyplayer/' + pos + '/' + playertoreplace)
    }
    $scope.posfilter = function (pos) {
        return function (item) {
            return item.position === pos;
        };
    }

    vm.showPopup = function (title, msg) {
        $ionicPopup.alert({
            title: title,
            template: msg,
            buttons: [{
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                }]
        });
    };
    $scope.resetTeam = function () {
        playerbuyservice.resetPlayers()
        $scope.getTempPlayers();
    };
    $scope.confirmTeam = function () {
        //check team complete

        var err = false;
        angular.forEach($scope.mytempplayers, function (item, i) {
            if (item.name == '' && !err) {
                vm.showPopup('Oops...', 'Please complete your squad before continue')
                err = true;
            }
        });
        if (!err) {
            var data = {
                'players': $scope.mytempplayers
            }
            entryService.confirmTeam(data, $.jStorage.get("entry_id")).then(function () {
                vm.showPopup('Awesome!', 'Your team is ready!')
            }, function (error) {
                vm.showPopup('Oops...', error.data.result.message)

            })
        }
    };
    $scope.getTempPlayers = function () {
        playerbuyservice.getPlayers()
        $scope.teamvalue = playerbuyservice.getValue()
        $scope.mytempplayers = playerbuyservice.getPlayers()

        $scope.myteam.bank = parseFloat($scope.myteam.bank) - $scope.teamvalue

    };

    $scope.getTempPlayers();


});