﻿app.controller('NotificationCtrl', function ($ionicLoading, $ionicPopup, $timeout, $location, $scope, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform, NotificationSyncService, NotificationService) {
    var vm = this;

    vm.action = function (item) {
        console.log(item)
        if (item.action == 'grp_inv') {
            $ionicPopup.alert({
                title: item.message,
                template: '',
                buttons: [{
                        text: '<b>Accept</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            $ionicLoading.show()
                            var data = {
                                'status': 'approved',
                                'group_invitation_id': item.id
                            }
                            NotificationSyncService.updateGroupInvitation(data).then(function () {
                                vm.fetchFromDb()
                                $ionicLoading.hide()

                            })
                        }
                    },
                    {
                        text: '<b>Reject</b>',
                        type: 'button-negative',
                        onTap: function (e) {
                            $ionicLoading.show()
                            var data = {
                                'status': 'rejected',
                                'group_invitation_id': item.id
                            }
                            NotificationSyncService.updateGroupInvitation(data).then(function () {
                                vm.fetchFromDb()
                                $ionicLoading.hide()
                            })
                        }
                    }
                ]
            });
        } else if (item.action == 'grp_req') {
            $ionicPopup.alert({
                title: item.message,
                template: '',
                buttons: [{
                        text: '<b>Accept</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            $ionicLoading.show()
                            var data = {
                                'status': 'approved',
                                'group_request_id': item.id
                            }
                            NotificationSyncService.updateGroupRequest(data).then(function () {
                                vm.fetchFromDb()
                                $ionicLoading.hide()

                            })
                        }
                    },
                    {
                        text: '<b>Reject</b>',
                        type: 'button-negative',
                        onTap: function (e) {
                            $ionicLoading.show()
                            var data = {
                                'status': 'rejected',
                                'group_request_id': item.id
                            }
                            NotificationSyncService.updateGroupRequest(data).then(function () {
                                vm.fetchFromDb()
                                $ionicLoading.hide()
                            })
                        }
                    }
                ]
            });
        }
    }

    vm.fetchFromDb = function () {
        vm.notificationLists = {}
        NotificationSyncService.groupInvitation().then().then(function () {
            vm.callDb();
            $scope.$broadcast('scroll.refreshComplete');
        })
    }

    vm.callDb = function () {
        NotificationService.all().then().then(function (data) {
            $ionicLoading.hide()
            console.log(data)
            vm.notificationLists = data;

            $timeout(function () {
                ionicMaterialMotion.fadeSlideIn({
                    selector: '.animate-fade-slide-in .item'
                });
            }, 200);

            // Activate ink for controller
            ionicMaterialInk.displayEffect();

        })
    }


    $ionicPlatform.ready(function () {
        $ionicLoading.show()
        vm.fetchFromDb()

    })


});