service.factory('NotificationSyncService', function ($q, $http, $cordovaSQLite) {

    this.groupInvitation = function (data, callback, err) {
        try {
            var q = $q.defer();
            if (window.cordova) {
                console.log("Opening database on device");
                var db = $cordovaSQLite.openDB("minicms.db");
            } else {
                console.log("Opening database on web");
                var db = window.openDatabase("minicms.db", '1', 'my', 1024 * 1024 * 100);
            }
        } catch (error) {
            console.log(error)
        }

        var m = $q.defer();
        $http({
            url: adminurl + 'group/pendinganswer',
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + $.jStorage.get('token', ''),
            },
            data: data
        }).success(function (response) {
            console.log('group/pendinganswer api ' + response)
            $cordovaSQLite.execute(db, "DELETE FROM notification WHERE action = 'grp_inv'").then(function (res) {
                if (response.result.length > 0) {
                    angular.forEach(response.result, function (item) {
                        console.log(item.name)
                        var query = "INSERT INTO notification (id,message,image,action,created_at) VALUES (?,?,?,?,?)";
                        $cordovaSQLite.execute(db, query, [item.id, item.message, item.image, 'grp_inv', item.created_at.date]).then(function (result) {
                            m.resolve(result)
                        }, function (error) {
                            console.log(error)
                            m.reject(error)
                        });
                    });
                } else {
                    m.resolve();
                }
            }, function (error) {
                console.log(error)
            });

        })
        return m.promise;

    }
    
     this.groupRequest = function (data, callback, err) {
        try {
            var q = $q.defer();
            if (window.cordova) {
                console.log("Opening database on device");
                var db = $cordovaSQLite.openDB("minicms.db");
            } else {
                console.log("Opening database on web");
                var db = window.openDatabase("minicms.db", '1', 'my', 1024 * 1024 * 100);
            }
        } catch (error) {
            console.log(error)
        }

        var m = $q.defer();
        $http({
            url: adminurl + 'group/pendingrequestanswer',
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + $.jStorage.get('token', ''),
            },
            data: data
        }).success(function (response) {
            console.log('group/pendingrequestanswer api ' + response)
            $cordovaSQLite.execute(db, "DELETE FROM notification WHERE action = 'grp_req' ").then(function (res) {
                if (response.result.length > 0) {
                    angular.forEach(response.result, function (item) {
                        console.log(item.name)
                        var query = "INSERT INTO notification (id,message,image,action,created_at) VALUES (?,?,?,?,?)";
                        $cordovaSQLite.execute(db, query, [item.id, item.message, item.image, 'grp_req', item.created_at.date]).then(function (result) {
                            m.resolve(result)
                        }, function (error) {
                            console.log(error)
                            m.reject(error)
                        });
                    });
                } else {
                    m.resolve();
                }
            }, function (error) {
                console.log(error)
            });

        })
        return m.promise;

    }

    this.updateGroupInvitation = function (data) {
        var m = $q.defer();

        $http({
            url: adminurl + 'group/update',
            method: "PUT",
            headers: {
                'Authorization': 'Bearer ' + $.jStorage.get('token', ''),
            },
            data: data
        }).success(function (response) {
            m.resolve();
        })

        return m.promise;
    }

    this.updateGroupRequest = function (data) {
        var m = $q.defer();

        $http({
            url: adminurl + 'group/requestupdate',
            method: "PUT",
            headers: {
                'Authorization': 'Bearer ' + $.jStorage.get('token', ''),
            },
            data: data
        }).success(function (response) {
            m.resolve();
        })

        return m.promise;
    }

    return this;
});
