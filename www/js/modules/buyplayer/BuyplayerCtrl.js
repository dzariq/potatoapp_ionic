﻿app.controller('BuyplayerCtrl', function (ligasuperService, myteam, playerbuyservice, teams, $stateParams, $ionicLoading, $ionicPopup, $timeout, $location, $scope, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform, NotificationSyncService, NotificationService) {
    console.log($stateParams.pos)

    $scope.myteam = myteam
    $scope.playerlist = ''
    $scope.teams = teams
    $scope.teamfilter = ''
    $scope.orderby = '-points'
    $scope.teamvalue = 0;
    $scope.banknow = $scope.myteam.bank

    $scope.getTempPlayers = function () {
        $scope.teamvalue = playerbuyservice.getValue()
        console.log($scope.teamvalue)
        var bank = $scope.myteam.bank
        bank = parseFloat($scope.myteam.bank) - parseFloat($scope.teamvalue)
        $scope.banknow = bank
    };

    $scope.getTempPlayers();


    if ($stateParams.pos == 'gk') {
        $scope.position = 'Goalkeepers';
    }
    if ($stateParams.pos == 'df') {
        $scope.position = 'Defenders';
    }
    if ($stateParams.pos == 'mf') {
        $scope.position = 'Midfielders';
    }
    if ($stateParams.pos == 'st') {
        $scope.position = 'Forwards';
    }

    $ionicLoading.show({
        template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
    });
    ligasuperService.getPlayers({'position': $stateParams.pos, 'entry_id': $.jStorage.get("entry_id")}).then(function (result) {
        $scope.playerlist = result.result;
        $ionicLoading.hide()
    }, function (e) {
        $ionicLoading.hide()

    })

    $scope.home = function () {
        $location.path('/app/home')
    }

    $scope.buyplayer = function (players) {
        playerbuyservice.setPlayer(players, $stateParams.playertoreplace).then(function (res) {
            showPopup('Cool!', res)
            if ($stateParams.playertoreplace != 0) {
                $location.path('/app/home')
            }
            //calculate price total of team
            var teamval = 0
            angular.forEach(playerbuyservice.getPlayers(), function (item, i) {
                if (item.name != '') {
                    teamval = parseFloat(teamval) + parseFloat(item.value)
                }
            });
            playerbuyservice.setValue(teamval)
            $scope.getTempPlayers();

        }, function (error) {
            showPopup('Oops', error)

        });

    }

    var showPopup = function (title, msg) {
        $ionicPopup.alert({
            title: title,
            template: msg,
            buttons: [{
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                }]
        });
    };
});