﻿app.controller('LineupCtrl', function (formations, entryService, $q, toaster, $ionicNavBarDelegate, $filter, lineup, formationService, $ionicLoading, myteam, myplayers, $ionicPopup, $timeout, $location, $scope, ionicMaterialMotion, ionicMaterialInk, $ionicPlatform, NotificationSyncService, NotificationService) {
    $timeout(function () {
        $ionicNavBarDelegate.showBackButton(false);
    }, 100);
    console.log(lineup)
    console.log(myteam)
    console.log(formations)

    //if not lineup set..set random lineup for starting
    if (lineup == 0) {
        var formationObject = $filter('filter')(formations, {'name': '442'})

        formationService.setFormationLayout(formationObject[0].name, myplayers, lineup).then(function (result) {
            console.log(result)
            $scope.gk = result.gk
            $scope.df = result.df
            $scope.mf = result.mf
            $scope.st = result.st
            $scope.subgk = result.subgk
            $scope.sub = result.sub
            $scope.formation = formationObject[0].name
            jQuery.each($scope.gk, function (i, item) {
                item.index = i
                item.parent = 'GK'
                item.inteam = true;
            })
            jQuery.each($scope.df, function (i, item) {
                item.index = i
                item.parent = 'DF'
                item.inteam = true;
            })
            jQuery.each($scope.mf, function (i, item) {
                item.index = i
                item.parent = 'MF'
                item.inteam = true;
            })
            jQuery.each($scope.st, function (i, item) {
                item.index = i
                item.parent = 'ST'
                item.inteam = true;
            })
            jQuery.each($scope.sub, function (i, item) {
                item.index = i
                item.parent = 'SUB'
                item.inteam = true;
            })
            jQuery.each($scope.subgk, function (i, item) {
                item.index = i
                item.parent = 'SUBGK'
                item.inteam = true;
            })

            //first time auto save
            $scope.saveLineup()
        })
    } else {
        lineup = JSON.parse(lineup)
        //display ready lineup
        var formationObject = $filter('filter')(formations, {'name': lineup.formation})

        formationService.setFormationLayout(formationObject[0].name, myplayers, lineup).then(function (result) {
            console.log(result)
            $scope.gk = result.gk
            $scope.df = result.df
            $scope.mf = result.mf
            $scope.st = result.st
            $scope.subgk = result.subgk
            $scope.sub = result.sub
            $scope.formation = formationObject[0].name
            jQuery.each($scope.gk, function (i, item) {
                item.index = i
                item.parent = 'GK'
                item.inteam = true;
            })
            jQuery.each($scope.df, function (i, item) {
                item.index = i
                item.parent = 'DF'
                item.inteam = true;
            })
            jQuery.each($scope.mf, function (i, item) {
                item.index = i
                item.parent = 'MF'
                item.inteam = true;
            })
            jQuery.each($scope.st, function (i, item) {
                item.index = i
                item.parent = 'ST'
                item.inteam = true;
            })
            jQuery.each($scope.sub, function (i, item) {
                item.index = i
                item.parent = 'SUB'
                item.inteam = true;
            })
            jQuery.each($scope.subgk, function (i, item) {
                item.index = i
                item.parent = 'SUBGK'
                item.inteam = true;
            })
        })

    }

    $scope.saveLineup = function () {
        var lineup = {}

        lineup.formation = this.formation;
        lineup.gk = angular.copy($scope.gk);
        lineup.subgk = angular.copy($scope.subgk);
        lineup.sub = angular.copy($scope.sub);
        lineup.df = angular.copy($scope.df);
        lineup.mf = angular.copy($scope.mf);
        lineup.st = angular.copy($scope.st);
//save lineup
        entryService.confirmPicks({'picks': lineup}, $.jStorage.get("entry_id"), $.jStorage.get("current_mw")).then(function (data) {
            console.log(data)
            toaster.pop('Lineup', "", 'Lineup Saved');

        }, function (error) {
            toaster.pop('Oops..', "", error.data.result.message);

            console.log(error)
        })
        $scope.saved = true;
    }




    $scope.beforeDrop = function (event, ui, index, placeholder) {
        var deferred = $q.defer();

        var player = angular.element(ui.draggable).scope().player;
        console.log(myteam.transfer_val)
        console.log(myteam.matchweek)
        if (myteam.transfer_val == 'OFF' || $.jStorage.get('current_mw') != myteam.matchweek) {
            deferred.reject()
        }
        if (placeholder.position != player.position) {
            deferred.reject()
        } else {
            deferred.resolve()

        }
        return deferred.promise;
    }

    $scope.transfer = function (id) {
        $location.path('/popupgeneral/transfer/'+id)

    }

    $scope.onDrop = function (event, ui, index, placeholder) {

        $scope.saved = false;
        var player = angular.element(ui.draggable).scope().player;
        console.log(placeholder)
        console.log(player)
        if (placeholder.parent == player.parent) {
            console.log(placeholder.parent)
            console.log($scope.$eval(placeholder.parent.toLowerCase()))
            $scope.$eval(placeholder.parent.toLowerCase())[placeholder.index] = angular.copy(player)
            $scope.$eval(player.parent.toLowerCase())[player.index] = angular.copy(placeholder)
            jQuery.each($scope.$eval(placeholder.parent.toLowerCase()), function (i, item) {
                item.parent = placeholder.parent
                item.inteam = true;
                item.index = i
            })
        } else {
            console.log(placeholder)
            var player = angular.element(ui.draggable).scope().player;
            console.log('placeholder', placeholder);
            console.log('player', player);
            //check same position
            var pos = ''
            if (player.position == 'GK') {
                var pos = 'Goalkeeper'
            }
            if (player.position == 'DF') {
                var pos = 'Defender'

            }
            if (player.position == 'MF') {
                var pos = 'Midfielder'

            }
            if (player.position == 'ST') {
                var pos = 'Striker'

            }
            if (player.position == 'SUB') {
                var pos = 'Sub'

            }


//            if (player.inteam && placeholder.type != 0) {
            console.log(placeholder)
            console.log(placeholder.index)
            console.log(player)
            console.log(player.index)

            if ($scope.$eval(placeholder.parent.toLowerCase())[placeholder.index] == undefined) {
                $scope.$eval(placeholder.parent.toLowerCase()).push(angular.copy(player))
            } else {
                $scope.$eval(placeholder.parent.toLowerCase())[placeholder.index] = angular.copy(player)
            }
            console.log($scope.$eval(placeholder.parent.toLowerCase()))

            jQuery.each($scope.$eval(placeholder.parent.toLowerCase()), function (i, item) {
                console.log(item)

                item.parent = placeholder.parent.toLowerCase()
                item.inteam = true;
                item.index = i
            })
            console.log($scope.$eval(placeholder.parent.toLowerCase()))

            if ($scope.$eval(player.parent.toLowerCase())[player.index] == undefined) {
                $scope.$eval(player.parent.toLowerCase()).push(angular.copy(placeholder))

            } else {
                $scope.$eval(player.parent.toLowerCase())[player.index] = angular.copy(placeholder)
            }

            jQuery.each($scope.$eval(player.parent.toLowerCase()), function (i, item) {
                item.parent = player.parent
                item.inteam = true;
                item.index = i
            })
            console.log($scope.$eval(player.parent.toLowerCase()))
            $scope.saveLineup()
//            } else {
//                alert('Invalid Swapping')
//                deferred.reject();
//            }
//            player.inteam = true;
//            player.parent = placeholder.parent;
//            player.index = index;
        }

        //   $scope.$eval(placeholder.parent).splice($scope.$eval(placeholder.parent).length - 1, 1)

    };
});