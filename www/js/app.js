// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', [
    'ionic',
    'toaster',
    'ngDragDrop',
    'ngCordova',
    'multipleDatePicker',
    'angularMoment',
    'ionic-material',
    'starter.services'
]);
//var adminurl = 'http://localhost/potatoapp_mediator/api/public/api/'
var adminurl = 'http://128.199.227.29:555/api/'
var ligasuper_path = 'http://128.199.227.29:888/api/'
var credentials = ''
var socialShare = {};
app.run(function ($ionicPlatform, $state, $location, $ionicPopup, TableService) {
    $ionicPlatform.registerBackButtonAction(function (event) {
        if ($state.current.name == "app.home") {
            navigator.app.exitApp();
        } else {
            navigator.app.backHistory();
        }

    }, 100);
    $ionicPlatform.ready(function () {

//        if (window && window.plugins && window.plugins.socialsharing && window.plugins.socialsharing.share) {
//            socialShare = window.plugins.socialsharing.share;
//        }

        var isCordovaApp = document.URL.indexOf('http://') === -1
        if (isCordovaApp) {
            FCMPlugin.onNotification(
                    function (data) {
                        if (data.wasTapped) {
                            //Notification was received on device tray and tapped by the user. 
                            //     alert(JSON.stringify(data));
                        } else {
                            //Notification was received in foreground. Maybe the user needs to be notified. 
                            console.log(data)
                        }
                        if ($.jStorage.get('logged', 0) == 1) {
                            $location.path('/app/notifications')
                        } else {
                            $location.path('/app/login')
                        }
                    },
                    function (msg) {
                        console.log('onNotification callback successfully registered: ' + msg);
                    },
                    function (err) {
                        console.log('Error registering onNotification callback: ' + err);
                    }
            );
        }
//        if (window.cordova && window.cordova.plugins.Keyboard) {
//            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
//        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }

//create tables sqlite
        TableService.create(function () {

        }, function (err) {
            console.log(err)
            alert(err)
        });
    });
})


app.config(function ($stateProvider, $ionicConfigProvider, $urlRouterProvider) {
    $ionicConfigProvider.tabs.position('bottom');

//      $ionicConfigProvider.tabs.position('bottom');
    $stateProvider
            .state('app', {
                url: '/app',
                cache: false,
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl as appCtrl'
            })

            .state('popupgeneral', {
                url: '/popupgeneral',
                cache: false,
                abstract: true,
                templateUrl: 'templates/popup-general.html',
                controller: 'AppCtrl as appCtrl'
            })

            .state('page', {
                url: '/page',
                cache: false,
                abstract: true,
                templateUrl: 'templates/page.html',
                controller: 'AppCtrl as appCtrl'
            })

            .state('popupgeneral.login', {
                url: '/login',
                resolve: {
                    "logged": function ($location) {
//                        var m = $q.defer();
                        if ($.jStorage.get("logged", 0) == 1) {
                            $location.path('/app/home')
                        }
//                        return m.promise;
                    }
                },
                views: {
                    'popup': {
                        templateUrl: 'js/modules/auth/login.html',
                        controller: 'AuthCtrl as authCtrl'
                    }
                }
            })

            .state('popupgeneral.buyplayer', {
                url: '/buyplayer/:pos/:playertoreplace',
                resolve: {
                    teams: function (ligasuperService, $q, $location) {
                        var m = $q.defer();
                        ligasuperService.getTeams().then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    myteam: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getTeam($.jStorage.get("entry_id")).then(function (response) {
                            $.jStorage.set("current_mw", response.result.matchweek);
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    }
                },
                views: {
                    'popup': {
                        templateUrl: 'js/modules/buyplayer/buyplayer.html',
                        controller: 'BuyplayerCtrl as buyplayerCtrl'
                    }
                }
            })

            .state('app.home', {
                url: '/home',
                resolve: {
                    myplayers: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getPlayers($.jStorage.get("entry_id")).then(function (response) {
                            if (response.result) {
                                $location.path('/app/lineup')
                            }
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    myteam: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getTeam($.jStorage.get("entry_id")).then(function (response) {
                            $.jStorage.set("current_mw", response.result.matchweek);
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    }
                },
                views: {
                    'home-tab': {
                        templateUrl: 'js/modules/home/home.html',
                        controller: 'HomeCtrl as homeCtrl'
                    }
                }
            })

            .state('app.lineup', {
                url: '/lineup',
                resolve: {
                    myplayers: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getPlayers($.jStorage.get("entry_id")).then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    myteam: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getTeam($.jStorage.get("entry_id")).then(function (response) {
                            $.jStorage.set("current_mw", response.result.matchweek);
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    lineup: function (entryService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        entryService.getPicks($.jStorage.get("entry_id"), '').then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    formations: function (formationService, $q, $location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                        var m = $q.defer();
                        formationService.getFormations().then(function (response) {
                            m.resolve(response);
                        })

                        return m.promise;
                    }
                },
                views: {
                    'home-tab': {
                        templateUrl: 'js/modules/lineup/lineup.html',
                        controller: 'LineupCtrl as lineupCtrl'
                    }
                }
            })

            .state('popupgeneral.signup', {
                url: '/signup',
                resolve: {
                    "logged": function ($location) {
//                        var m = $q.defer();
                        if ($.jStorage.get("logged", 0) == 1) {
                            $location.path('/app/home')
                        }
//                        return m.promise;
                    }
                },
                views: {
                    'popup': {
                        templateUrl: 'js/modules/auth/signup.html',
                        controller: 'AuthCtrl as authCtrl'
                    }
                }
            })
            .state('popupgeneral.transfer', {
                url: '/transfer/{playerid}',
                resolve: {
                    "logged": function ($location) {
                        if ($.jStorage.get("logged", 0) == 0) {
                            $location.path('/popupgeneral/login')
                        }
                    },
                    teams: function (ligasuperService, $q, $location) {
                        var m = $q.defer();
                        ligasuperService.getTeams().then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    player: function (ligasuperService, $q, $stateParams) {
                        var m = $q.defer();
                        ligasuperService.getPlayer($stateParams.playerid).then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    myplayers: function (entryService, $q, $location) {
                        var m = $q.defer();
                        entryService.getPlayers($.jStorage.get("entry_id")).then(function (response) {
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    },
                    myteam: function (entryService, $q, $location) {
                        var m = $q.defer();
                        entryService.getTeam($.jStorage.get("entry_id")).then(function (response) {
                            $.jStorage.set("current_mw", response.result.matchweek);
                            m.resolve(response.result);
                        }, function (error) {
                            m.reject(error);
                        })

                        return m.promise;
                    }
                },

                views: {
                    'popup': {
                        templateUrl: 'js/modules/transfer/transfer.html',
                        controller: 'TransferCtrl as transferCtrl'
                    }
                }
            })

            .state('popupingroup.notifications', {
                url: '/notifications',
                cache: false,
                views: {
                    'popup': {
                        templateUrl: 'js/modules/notifications/notification.html',
                        controller: 'NotificationCtrl as notificationCtrl'
                    }
                }
            })

    $urlRouterProvider.otherwise('/popupgeneral/login');
});
var service = angular.module('starter.services', []);
