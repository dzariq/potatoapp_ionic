﻿app.controller('AppCtrl', function ($scope, $ionicLoading, $ionicPopup, AuthService, $cordovaSocialSharing, $ionicPlatform, $window, $ionicHistory, $location, $ionicModal, $ionicPopover, $timeout) {
    // Form data for the login modal
    $scope.loginData = {};
    var vm = this;
    jQuery.noConflict()
    vm.profile = [];

    if ($.jStorage.get('logged', 0) == 1) {
        vm.islogged = true
    }

//    var navIcons = document.getElementsByClassName('ion-navicon');
//    for (var i = 0; i < navIcons.length; i++) {
//        navIcons.addEventListener('click', function () {
//            this.classList.toggle('active');
//        });
//    }

    vm.share = function () {
        var message = "Introducing Groupliner™ - gather and engage the world. Now available on App Store & Google Play. Don’t forget to share it with your friends.";
        var subject = "Groupliner™";
        var image = "";
        var url = "http://store.diwebsite.com/androidv1.apk";

        $cordovaSocialSharing.share(message, subject, image, url).then(function (result) {
            // Success!
        }, function (err) {
            // An error occurred. Show a message to the user
        });

    }

    vm.login = function () {
        $location.path('/popupgeneral/login')

    }

    vm.signup = function () {
        $location.path('/popupgeneral/signup')

    }

    vm.logout = function () {
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });
        AuthService.logout().then(function (result) {
            $.jStorage.flush();
            $ionicLoading.hide()
            vm.showPopup('Logged Out')
            $location.path('/app/login')
            $location.path('/popupgeneral/login')
        }, function (error) {
            console.log(error)
            $ionicLoading.hide()

            alert(error.data.result.message)

        })


    }



    vm.showPopup = function (title, msg) {
        $ionicPopup.alert({
            title: title,
            template: msg,
            buttons: [{
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                }]
        });
    };

    var fab = document.getElementById('fab');
//    fab.addEventListener('click', function () {
//        //location.href = 'https://twitter.com/satish_vr2011';
//        window.open('https://twitter.com/satish_vr2011', '_blank');
//    });

    // .fromTemplate() method
    var template = '<ion-popover-view>' +
            '   <ion-header-bar>' +
            '       <h1 class="title">Menu</h1>' +
            '   </ion-header-bar>' +
            '   <ion-content class="padding">' +
            '       <div ui-sref="app.editprofile">Edit Profile</div><br/><br/>' +
            '       <div ui-sref="app.notifications">Notifications</div>' +
            '   </ion-content>' +
            '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });
    $scope.closePopover = function () {
        $scope.popover.hide();
    };
    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.popover.remove();
    });
});